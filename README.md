#Used In
Used for Meet Magento India, Mage Titan Mexico sites

Wagento Speaker Module for Wagento
======================

This Module for Magento® 2 allows to Add New Speaker Type Product from Admin   

Facts
-----
* version: 2.0.0

Description
-----------
01/12/18

* This module is used to Add Speaker Type Product And New Speaker Type Attribute set From Admin.
 Custom Attribute For Speaker Type Product and Speaker Attribute Set
  * Keynote Speaker
  * Speaker Title
  * Website URL
  * Twitter
  * Talk Title
  * Workshop Title
  * Talk Synopsis
  * View Slides

Requirements
------------
* PHP >= 5.6.5

Compatibility
-------------
* Magento >= 2.1.4

Installation Instructions
-------------------------
The Wagento Sponsors module for Magento® 2 is distributed in three formats:
* Drop-In
* [Composer VCS](https://getcomposer.org/doc/05-repositories.md#using-private-repositories)

### Install Source Files ###

The following sections describe how to install the module source files,
depending on the distribution format, to your Magento® 2 instance.


Then navigate to the project root directory and run the following commands:

    composer config repositories.wagento-speaker vcs git@bitbucket.org:wagento-global/module-speaker.git
    composer require wagento/speaker:dev-master

#### VCS ####
If you prefer to install the module using [git](https://git-scm.com/), run the
following commands in your project root directory:

    composer config repositories.wagento-speaker vcs git@bitbucket.org:wagento-global/module-speaker.git
    composer require wagento/speaker:dev-master

### Enable Module ###
Once the source files are available, make them known to the application:

    ./bin/magento module:enable Wagento_Speaker
    ./bin/magento setup:upgrade

Last but not least, flush cache and compile.

    ./bin/magento cache:flush
    ./bin/magento setup:di:compile

Uninstallation
--------------

The following sections describe how to uninstall the module, depending on the
distribution format, from your Magento® 2 instance.

#### Composer Git ####

To unregister the shipping module from the application, run the following command:

    ./bin/magento module:uninstall --remove-data Wagento_Speaker

This will automatically remove source files and clean up the database.

#### Drop-In ####

To uninstall the module manually, run the following commands in your project
root directory:

    ./bin/magento module:disable Wagento_Speaker
    rm -r app/code/Wagento/Speaker

Features
--------------


#### Admin Settings ####

1) For Made any Product as Speaker type.

For Create Event type Product -> Go to Admin -> Products ->Catalog -> Add Product from dropdown "Speaker Product"
 then choose "speaker" Attribute set.


     
Developer
---------
* Dasharath Patel | [Wagento](https://www.wagento.com/) | dasharath@wagento.com

License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)